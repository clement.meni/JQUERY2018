// jQuery(document).ready(function(){});
// $(document).ready(function(){});
$(function(){
	// ici tout le jquery
	// $('selecteur css').methodeJquery(...args?);

	// $('selecteur css').event(function(...args?){
	// 	$('selecteur css').methodeJquery()(...args?);
	// });

	$('button:first-of-type').click(function(){
		// masquer tous les p du dom
		$('p').hide(400);
	});

	$('button:nth-of-type(2)').click(function(){
		// afficher tous les p du dom
		$('p').show(400);
	});

	$('button:nth-of-type(3)').click(function(){
		// masquer le premier li de ul
		$('ul li:first-child').hide(400);
	});

	$('button:nth-of-type(4)').click(function(){
		// masquer tous les éléments du dom qui ont un attribut href
		$('[href]').hide(400);
	});

	$('button:nth-of-type(5)').click(function(){
		// afficher tous les éléments du dom qui ont l'attribut href et la valeur http://orsys.fr
		$('[href="http://orsys.fr"]').show(400);
	});

	$('button:nth-of-type(6)').click(function(){
		// masquer tous les éléments du dom qui ont l'attribut href et une valeur qui contient http
		$('[href*="http"]').hide(400);
	});
});