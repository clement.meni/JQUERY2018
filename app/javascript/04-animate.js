$(function(){

	$('button:first-of-type').click(function(){
		// position elem en left 500 avec une animation qui dure 1s
		$('.blue').animate({'left': '500px'}, 1000);
		$('.red').animate({'left': '500px'}, 800);
		$('.green').animate({'left': '500px'}, 1200);
	});

	$('button:nth-of-type(5)').click(function(){
		// position elem en left 500 avec une animation qui dure 1s
		$('div').animate({
			'left': '0px',
			'top': 0,
			'width': '100px',
			'height': '100px'
		}, 400);
	});

	$('button:nth-of-type(2)').click(function(){
		// animate on multiple css properties
		$('.blue').animate({
			'left': 500,
			'top': 500,
			'backgroundColor' : '#ffb612',
			'width': '200px',
			'height': '200px'
		}, 1000);
	});

	$('button:nth-of-type(3)').click(function(){
		// animate on multiple css properties
		$('.blue')
		.animate({
			'left': 500,
			'top': 500,
			'width': '200px',
			'height': '200px'
		}, 1000)
		.hide()
		.show(1000)
		.animate({
			'left': '0px',
			'top': 0,
			'width': '100px',
			'height': '100px'
		}, 400);
	});


	// Exercice

	$('button:nth-of-type(4)').click(function(){
		$('.blue').animate({
			'left': '1000px',
			'top': '0px',
		}, 800)
		.animate({
			'left': '1000px',
			'bottom': '0px',
		}, 1200)
		.animate({
			'right': '1000px',
			'bottom': '0px',
		}, 1200);
		});
	});