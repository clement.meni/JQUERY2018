$(function () {
    //  blur = quand il perd le focus
    $('[data-form] [name]').blur(function () {
        // si la valeur d'un champ est vide...
        if ($(this).val() == '') {
            // ...on ajoute la class 'invalid' sur le label qui suit ce champ
            $(this).next().addClass('invalid').removeClass('valid');
        } else {
            // on ajoute la class 'valid' sur le label qui suit ce champ...
            //  ...et on retire la class 'invalid' si elle est dessus
            $(this).next().addClass('valid').removeClass('invalid');
        }
    });

    $('[data-form]').submit(function(){
        let valid = true;
        $('[data-form] [required]').each(function () {
            if ($(this).val() == ''){
                valid = false;
                $(this).next().addClass('invalid').removeClass('valid');
            }
        });
        if (valid) {
            $.ajax({
                type: "POST",
                url: "envoimail.php",
                // permet de passer tout ce que le user a saisi => en objet .json
                data: $(this).serialize(),
                success: function (response) {
                    if (response == 'Formulaire invalide'){
                        // traitre la réponse
                        // add class invalid sur champs mal rempli
                        // affiche text echec
                        $('[data-form] .popup').text('champ invalid').fadeIn();
                    } else {
                        // traite la réponse
                        $('[data-form] [name]').each(function(){
                            $(this).val() = '';
                        });
                        $('[data-form] .popup').text('envoi réussi').fadeIn();
                    }
                }
            });
        }
        return false;
    });

});