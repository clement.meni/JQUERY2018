$(function () {

    let nbImg;
    let index;
    let src;

    nbImg = $('[data-galery] img').length;

    let changeImg = function () {
        // get attr src de img suivante
        src = $('[data-galery] img').eq(index).attr('src');
        // set attr src img lightbox
        $('[data-lightbox] img').attr('src', src);
        changePuce();
    }

    // générer autant de puces qu'il y a d'img dans la galery
    let generatePuces = function () {
        for (i = 0; i < nbImg; i++) {
            $('[data-lightbox] ul').append('<li><i class="icon-circle-o"></i></li>');
        }
    }

    // change puces pour colorier la puce de l'image sélectionnée
    let changePuce = function () {
        $('[data-lightbox] i')
            .removeClass('icon-circle')
            .addClass('icon-circle-o')
            .eq(index)
            .addClass('icon-circle')
            .removeClass('icon-circle-o');
    }

    generatePuces();


    // Quand on clique sur une image on ouvre la lightbox
    $('[data-galery] img').click(function () {
        // get index img cliquée
        index = $('[data-galery] img').index($(this));
        // get attr src img cliquée
        src = $(this).attr('src');
        // set attr src img lightbox
        $('[data-lightbox] img').attr('src', src);
        // ouvre la lightbox
        $('[data-lightbox]').fadeIn().css({
            'display': 'flex'
        });
        // colorise la puce qui correspond à l'image cliquée
        changePuce();
    });

    // fermer la lightbox quand on clique sur icon-close
    $('[data-lightbox] .icon-close').click(function () {
        $('[data-lightbox]').fadeOut();
    });

    // quand on clique sur icon-angle-right
    $('[data-lightbox] .icon-angle-right').click(function () {
        index = (index + 1) % nbImg;
        changeImg();
    });

    // quand on clique sur icon-angle-left
    $('[data-lightbox] .icon-angle-left').click(function () {
        index = (index - 1) % nbImg; // passe en négatif, fonctionne car tableau parcouru à l'envers
        index = (index - 1 + nbImg) % nbImg; // permet de rester en positif
        changeImg();
    });

    // quand on clique sur une puce
        // on récupère l'index de la puce cliquée et on enregistre dans la variable index
        // changeImg()
    $('[data-lightbox] i').click(function(){
        index = $('[data-lightbox] i').index($(this));
        changeImg();
    });
    



});