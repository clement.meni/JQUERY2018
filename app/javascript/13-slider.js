$(function () {
    // Au chargement de la page, on autorise le user à cliquer sur un btn pour défiler à gauche ou à droite
    let acceptDefil = true;

    // récupérer la largeur de la page pour l'affecter aux li
    let l = $('[data-slider]').width();
    $('[data-slider] li').width(l);

    // 1. Quand on resize la fenêtre
    // 1.1 recalculer la largeur du slider
    // 1.2 ré-affecter cette largeur aux li
    $(window).resize(function () {
        let l = $('[data-slider]').width();
        $('[data-slider] li').width(l);
    });

    let animation = function(){
        $('[data-slider] .progress').animate({
            'width': '100%'
        }, 2000, function () {
            $(this).css({
                'width': '0%'
            });
        });
    }

    // 1. créer une fonction pour : animer ul (slider.width) - (ul.width)
    // 2. une fois animation terminée : 
    // 2.1 mettre 1er li après le dernier li
    // 2.2 repositionner le ul en left 0

    let defilRight = function () {
        $('[data-slider] ul').animate({
            'left': -l
        }, 1000, function () {
            $('[data-slider] li:last').after($('[data-slider] li:first'));
            $(this).css({
                'left': 0
            });
            acceptDefil = true;
            animation();
        });
    }

    // 1. Positionner ul en left -? sans animation
    // 2. dans le même temps, mettre dernier li devant 1er li
    // 3. animer ul de left -? vers left 0 en 1s

    let defilLeft = function () {
        $('[data-slider] ul').css({
            'left': -l
        });
        $('[data-slider] li:first').before($('[data-slider] li:last'));
        $('[data-slider] ul').animate({
            'left': 0
        }, 1000, function () {
            acceptDefil = true;
        });
        animation();
    }




    // Quand on clique sur le btn left : 
    // 1. Stopper le défilement automatique
    // 2. Quand on clique sur le btn left on veut appeler une fois defilLeft
    // 3. on accepte le défilment si l'animation est terminée

    $('[data-slider] .icon-angle-left').click(function () {
        if (acceptDefil) {
            acceptDefil = false;
            clearInterval(interval);
            defilLeft();
        }
    });

    // Quand on clique sur le btn right : 
    // 1. Stopper le défilement automatique
    // 2. Quand on clique sur le btn right on veut appeler une fois defilRight

    $('[data-slider] .icon-angle-right').click(function () {
        if (acceptDefil) {
            acceptDefil = false;
            clearInterval(interval);
            defilRight();
        }
    });

    let interval = setInterval(defilRight, 3000);
});