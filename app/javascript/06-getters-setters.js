$(function(){

	$('button:first-of-type').click(function(){
		let text = $('.text').text();
		alert(text);
	});

	$('button:nth-of-type(2)').click(function(){
		$('.text').text('Hello !');
	});

	$('button:nth-of-type(3)').click(function(){
		let html = $('.html').html();
		alert(html);
	});

	$('button:nth-of-type(4)').click(function(){
		$('.html').html('<strong>Hello</strong> !');
	});

	$('button:nth-of-type(5)').click(function(){
		let attr = $('img').attr('src');
		alert(attr);
	});

	$('button:nth-of-type(6)').click(function(){
		// permet de remplacer une image par une autre
		$('img').attr('src', 'img/macbook.jpg');
	});

	$('button:nth-of-type(7)').click(function(){
		let val = $('#name').val();
		alert(val);
	});

	$('button:nth-of-type(8)').click(function(){
		$('#name').val('Insert your name');
	});




});