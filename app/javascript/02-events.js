$(function(){

	$('button:first-of-type').click(function(){
		// click pour modifier le texte du premier p
		$('p:first-of-type').text('Hello !');
	});

	$('button:nth-of-type(2)').dblclick(function(){
		// double click pour modifier le texte du premier p
		$('p:first-of-type').text('Hola !');
	});

	$('button:nth-of-type(3)').mouseenter(function(){
		// mouse enter pour modifier le texte du premier p
		$('p:first-of-type').text('Hallo !');
	});

	$('button:nth-of-type(4)').mouseleave(function(){
		// mouse leave pour modifier le texte du premier p
		$('p:first-of-type').text('Bonjour !');
	});

	$('button:nth-of-type(5)').hover(function(){
		// hover pour modifier le texte du premier p
		$('p:first-of-type').text('Je suis un hover !');
	}, function(){
		$('p:first-of-type').text('Je suis un mouse leave !');
	});

	$('input').focus(function(){
		$(this).val('ce champ prend le focus');
	});

	$('input').blur(function(){
		$(this).val('no focus');
	});

	$(window).keyup(function(){
		$('p:first-of-type').text('keyup works !');
	});




});