$(function () {

    // 1. Au chargement de la page définir un maxlength
    // 2. modifier attr maxlength du textarea
    // 3. modifier le texte de la balise small
    // 4. dès que l'utilisateur saisi une lettre on veut compter le nombre de caractère saisies
    // 5. on veut soustraire ce nombre à max

    let max = 10;
    let lChaine;
    let pastedData;

    $('[compteur] textarea').attr('maxlength', max);
    $('[compteur] small').text(max);

    $('[compteur] textarea').keyup(function () {
        lChaine = $(this).val().length;
        $('[compteur] small').text(max - lChaine);
    });

    // Pour les copier/coller
    
    $('[compteur] textarea').bind('paste', function(e){
        pastedData = e.originalEvent.clipboardData.getData('text');
        lChaine = pastedData.length + $(this).val().length;
        if (lChaine < max) {
            $('[compteur] small').text(max - lChaine);
        } else {
            $('[compteur] small').text(0);
        }
        
    });

});