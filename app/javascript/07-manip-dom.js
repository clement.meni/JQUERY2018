$(function(){

	$('button:first-of-type').click(function(){
		$('ul').before('<p>Before</p>');
	});

	$('button:nth-of-type(2)').click(function(){
		$('ul').after('<p>After</p>');
	});

	$('button:nth-of-type(3)').click(function(){
		$('ul').prepend('<li>Item ajouté par prepend</li>');
	});

	$('button:nth-of-type(4)').click(function(){
		$('ul').append('<li>Item ajouté par append</li>');
	});

	let compteur = 0;

	$('button:nth-of-type(5)').click(function(){
		compteur ++;
		if (compteur <= 7){
			$('ul').append('<li>Item ajouté par append</li>');
		} else {
			alert('Quota atteint')
		}
	});

	let compt = 1

	$('button:nth-of-type(6)').click(function(){
		compt --;
		if (compt < 0){
			alert('Quota atteint')
		} else {
			$('ul').prepend('<li>Item ajouté par append</li>');
		}
	});

	let i = 3;

	$('button:nth-of-type(7)').click(function(){
		i++;
		if (i <= 10){
			$('ul').append('<li>item ' + i + '</li>');
		} else {
			alert('Limite atteinte');
		}
	})

	let pre = 1

	$('button:nth-of-type(8)').click(function(){
		pre--;
		if (pre < 0){
			alert('Quota atteint');
		} else {
			$('ul').prepend('<li>Item ' + pre + '</li>');
		}
	});

	

	// Exercice : récupérer le dernier caractère du dernier li
	// Exercice : convertir le dernier caractère en nombre
	// Exercice : assigner cette valeur à la variable k


	let chaine = $('li').last().text(); // return item 3
	let lastChar = parseInt(chaine[chaine.length - 1 ]); // return 3 (int)
	let k = lastChar;
	

	$('button:nth-of-type(9)').click(function(){
		k++;
		if (k <= 10){
			$('ul').append('<li>item ' + k + '</li>');
		} else {
			alert('Limite atteinte');
		}
	});

	// Exercice : récupérer le dernier caractère du 1er li
	// Exercice : convertir le dernier caractère en nombre
	// Exercice : assigner cette valeur à la variable l

	let chaine2 = $('li').first().text(); // return item 1
	let lastChar2 = parseInt(chaine2[chaine2.length - 1 ]); //return 1 (int)
	let l = lastChar2;
	

	$('button:nth-of-type(10)').click(function(){
		l--;
		if (l < 0){
			alert('Limite atteinte');
		} else {
			$('ul').prepend('<li>item ' + l + '</li>');
		}
	});





});