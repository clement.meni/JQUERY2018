$(function(){

    // 1. quand on clique sur une question, on referme toutes les réponses
    // 2. on ouvre la réponse juste après la question cliquée
    // 3. mettre la class icon-angle-down sur tous les span
    // 4. mettre la class icon-angle-up sur le span qui correspond à la question
    // 5. supprimer la class active
    // 6. ajouter la class active 

	$('[accordeon] h1').click(function(){
        $('[accordeon] p').slideUp(400);
        $(this).next().slideDown(400);
        $('[accordeon] i').attr('class', 'icon-angle-down');
        $(this).children('i').attr('class', 'icon-angle-up');
        $('[accordeon] h1').removeClass('active'); // il effectue d'abord cette vérification
        $(this).addClass('active');  // ... avant d'effectuer cette action
    });

});